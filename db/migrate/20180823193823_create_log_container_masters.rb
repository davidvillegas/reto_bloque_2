class CreateLogContainerMasters < ActiveRecord::Migration[5.2]
  def change
    create_table :log_container_masters do |t|
      t.references :container, foreign_key: true, null: false
      t.references :bl_master, foreign_key: true, null: false
      t.date :date, null: false
    end
  end
end
