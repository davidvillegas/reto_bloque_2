class CreateTravels < ActiveRecord::Migration[5.2]
  def change
    create_table :travels do |t|
      t.references :vessel, foreign_key: true, null: false
      t.integer :origin, null: false
      t.integer :destination, null: false
      #t.references :origin, index: true
      #t.references :destination, index: true
      t.date :date, null: false
    end
  end
end
