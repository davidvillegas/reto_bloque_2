
50.times do |index|
  ship = ShippingCompany.all.sample
  Vessel.create!(name: Faker::Name.name,
                 shipping_company_id: ship.id)
end
