
def convertidor(num)
  ('A'..'Z').to_a.sample(num).join
end
kin = ['ST','HQ']
siz = [20,40]
50.times do |index|
  Container.create!(code: convertidor(4)+Faker::Number.number(7).upcase,
                    size: siz.sample,
                    kind: kin.sample)
end
