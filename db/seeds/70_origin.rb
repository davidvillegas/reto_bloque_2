
50.times do |index|
  nvo = Nvocc.all.sample
  consig = Consignee.all.sample
  pais = Country.all.sample
  Origin.create!(nvocc_id: nvo.id,
                 consignee_id: consig.id,
                 country_id: pais.id,
                 price: Faker::Number.decimal(5,2))
end
