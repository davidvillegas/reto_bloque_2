
50.times do |index|
  nvo = Nvocc.all.sample
  trav = Travel.all.sample
  ship = ShippingCompany.all.sample
  BlMaster.create!(shipping_company_id: ship.id,
                   nvocc_id: nvo.id,
                   travel_id: trav.id,
                   date: Faker::Time.between(4.month.ago, 1.week.ago))
end
