
50.times do |index|
  blm = BlMaster.all.sample
  consig = Consignee.all.sample
  BlHouse.create!(bl_master_id: blm.id,
                  consignee_id: consig.id,
                  date: Faker::Time.between(4.month.ago, 1.week.ago))
end
