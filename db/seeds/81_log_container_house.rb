
50.times do |index|
  container = Container.all.sample
  blh = BlHouse.all.sample
  LogContainerHouse.create!(container_id: container.id,
                             bl_house_id: blh.id,
                             date: Faker::Time.between(4.month.ago, 1.week.ago))
end
