class BlHouse < ActiveRecord::Base
  belongs_to :bl_master
  belongs_to :consignee
  has_many :log_container_houses
  has_many :containers, through: :log_container_houses
  validates :date, :bl_master_id, :consignee_id, presence: true



  private


end
