class BlMaster < ActiveRecord::Base
  belongs_to :shipping_company
  belongs_to :nvocc
  belongs_to :travel
  has_many :bl_houses, dependent: :destroy
  has_many :log_container_masters
  has_many :containers, through: :log_container_masters
  validates :date, :shipping_company_id, :nvocc_id, :travel_id, presence: true

 #-------------------------------------------------------#
          #Representación de los datos de un BL físico.
  def self.show_bl
    joins(:shipping_company, :nvocc, :travel, :log_container_masters).map(&:list_of_stuff).first
  end

  def list_of_stuff
    "ID: #{id} | Naviera: #{shipping_company.name} | NVOCC: #{nvocc.name} | Fecha de Atraque: #{date} | Origen: #{travel.origin} | Destino: #{travel.destination} | Id del Viaje: #{travel.id}" # N° Contenedor: #{log_container_masters.container_id} |
  end
  #-------------------------------------------------------#

  #-------------------------------------------------------#
          #Un listado todos los BL por naviera
  def self.list_per_sc
    joins(:shipping_company).group(:name).order(:id).map(&:convert)
    #joins(:shipping_company).group(:name).count
  end

  def convert
    "ID: #{id} | Naviera: #{shipping_company.name} | NVOCC: #{nvocc.name} | Fecha de Atraque: #{date} | Origen: #{travel.origin} | Destino: #{travel.destination} | Id del Viaje: #{travel.id}" # N° Contenedor: #{log_container_masters.container_id} |
  end
  #-------------------------------------------------------#

  private

end


