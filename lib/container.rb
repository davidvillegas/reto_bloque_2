class Container < ActiveRecord::Base
  has_many :log_container_masters
  has_many :bl_masters, through: :log_container_masters
  has_many :log_container_houses, dependent: :destroy
  has_many :bl_houses, through: :log_container_houses
  enum kind: {"Standard"=>'ST', "High-Cube"=>'HQ'}
  validates :code, :size, :kind, presence: true
  validates :code, uniqueness: true
  validates :code, length: { is: 11, message: "Caracteres Incompletos" }#validate :is_equal?
  validates :size, numericality: { only_integer: true }
  validate :handle_exception
  before_validation :entry_size #Metodo callback


    #----------------------------------#
    #Listado de Contenedores por tipos

    def self.find_types(kind)
      where("kind = ?", kind)
      handle_exception(kind)
    end

    def self.count_types(kind)
      where("kind = ?", kind).count
      #handle_exception(kind)
    end

    #----------------------------------#

  def self.handle_exception(var)
      find(var)
    rescue	ActiveRecord::RecordNotFound => e #ActiveRecord::NotNullViolation
      p "Registro No Encontrado"
      #puts "Registro No Encontrado"
  end

  private

  def entry_size
    errors.add(:size, "Solo Tamaños de 20 o 40 pies") unless (size == 20 || size == 40)
  end


  #No funciona
  def is_equal?
    errors.add(:code, "EL código debe contener 11 Caracteres") unless code.length == 11
  end

end

=begin

# Validaciones

  #validates :legacy_code, format: { with: /\A[a-zA-Z]+\z/,
    #message: "only allows letters" }
  #enum kind: [:ST, :HQ]
  #validates :campo, numericality: { only_integer: true }
  #validates :name, :login, :email, presence: true
  #validates :email, uniqueness: true, on: :create
  # OJO con la fecha validar que no sea mayor a Date.today

=end
