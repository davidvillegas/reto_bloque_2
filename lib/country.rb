class Country < ActiveRecord::Base
  has_one :travel
  has_many :origin
  validates :name, presence: true
  validates :name, uniqueness: { message: "El Nombre ya Existe en la Base de Datos" }
  validates :name, format: { with: /\A[a-zA-Z]+\z/, message: "Letras Unicamente Por Favor!" }

end
