class LogContainerHouse < ActiveRecord::Base
  belongs_to :container
  belongs_to :bl_house
  validates :date, :bl_house_id, :container_id, presence: true


  private

end
