require_relative '../db/connection'
require_relative 'container'
require_relative 'shipping_company'
require_relative 'vessel'
require_relative 'nvocc'
require_relative 'consignee'
require_relative 'country'
require_relative 'log_container_master'
require_relative 'origin'
require_relative 'travel'
require_relative 'bl_house'
require_relative 'bl_master'
require_relative 'log_container_house'

module Test

end
